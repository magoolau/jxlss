package pres.lnk.jxlss.command;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;
import org.jxls.area.Area;
import org.jxls.command.AbstractCommand;
import org.jxls.command.Command;
import org.jxls.common.CellRef;
import org.jxls.common.Context;
import org.jxls.common.Size;
import org.jxls.transform.poi.PoiTransformer;

/**
 * <p>合并单元格</p>
 * jx:merge(
 * lastCell="单元格"
 * [, cols="合并的列数"]
 * [, rows="合并的行数"]
 * [, minCols="最小合并的列数"]
 * [, minRows="最小合并的行数"]
 * )
 *
 * @Author lnk
 * @Date 2018/1/23
 * @deprecated 该命令类已经提交到官方代码中，请使用官方代码的 {@link org.jxls.command.MergeCellsCommand}
 */
@Deprecated
public class MergeCommand extends AbstractCommand {
    public static final String DEPRECATED_MSG = "该MergeCommand类已经提交到官方代码，请使用 org.jxls.command.MergeCellsCommand 代替";
    private String cols;        //合并的列数
    private String rows;        //合并的行数
    private String minCols;     //最小合并的列数
    private String minRows;     //最小合并的行数
    private CellStyle cellStyle;//第一个单元格的样式

    private Area area;

    @Override
    public String getName() {
        return "merge";
    }

    @Override
    public Command addArea(Area area) {
        if (super.getAreaList().size() >= 1) {
            throw new IllegalArgumentException("You can add only a single area to 'merge' command");
        }
        this.area = area;
        return super.addArea(area);
    }

    @Override
    public Size applyAt(CellRef cellRef, Context context) {
        throw new UnsupportedOperationException(DEPRECATED_MSG);
    }

    protected Size poiMerge(CellRef cellRef, Context context, PoiTransformer transformer, int rows, int cols) {
        throw new UnsupportedOperationException(DEPRECATED_MSG);
    }

    private static void setRegionStyle(CellStyle cs, CellRangeAddress region, Sheet sheet) {
        throw new UnsupportedOperationException(DEPRECATED_MSG);
    }

    public String getCols() {
        return cols;
    }

    public void setCols(String cols) {
        this.cols = cols;
    }

    public String getRows() {
        return rows;
    }

    public void setRows(String rows) {
        this.rows = rows;
    }

    public String getMinCols() {
        return minCols;
    }

    public void setMinCols(String minCols) {
        this.minCols = minCols;
    }

    public String getMinRows() {
        return minRows;
    }

    public void setMinRows(String minRows) {
        this.minRows = minRows;
    }
}
